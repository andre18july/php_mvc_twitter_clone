<?php

    namespace MF\Controller;

    abstract class Action{


        protected $view;

        public function __construct(){
            $this->view = new \stdClass();
        }

        protected function render($view, $layout = 'layout'){
            $this->view->page = $view;

            if(file_exists("../App/Views/". $layout . ".phtml")){
                require_once "../App/Views/". $layout . ".phtml";
            }else{
                $this->content();
            }
            
        }

        protected function content(){

            /*
            echo 'XXXXXXXXXXXXXXXXXXXXXXX';
            echo '<br>';
            echo get_class($this);
            echo '</br>';
            echo 'XXXXXXXXXXXXXXXXXXXXXXX';
            */


            $classAtual = get_class($this);
            $classAtual = str_replace('App\\Controllers\\', '', $classAtual);
            $classAtual = strtolower(str_replace('Controller', '', $classAtual));

            /*
            echo 'XXXXXXXXXXXXXXXXXXXXXXX';
            echo '<br>';
            echo $classAtual;
            echo '</br>';
            echo 'XXXXXXXXXXXXXXXXXXXXXXX';
            */


            require_once "../App/Views/". $classAtual . "/". $this->view->page .".phtml";
        }


    }


?>