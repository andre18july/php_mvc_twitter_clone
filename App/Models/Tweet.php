<?php

    namespace App\Models;

    use MF\Model\Model;


    class Tweet extends Model{

        private $id;
        private $id_utilizador;
        private $tweet;
        private $data;

    
        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }

        //guardar
        public function gravar(){
            $query = '
                insert 
                    into tweets(id_utilizador, tweet)
                values(:id_utilizador,:tweet)
            ';

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':id_utilizador', $this->__get('id_utilizador'));
            $stmt->bindValue(':tweet', $this->__get('tweet'));
            $stmt->execute();

            return $this;


        }

        //recuperar
        public function listar(){
            $query = '
                select  
                    t.id, t.id_utilizador, u.nome, t.tweet, DATE_FORMAT(t.data, "%d/%m/%Y %H:%i") as data 
                from 
                    tweets as t
                    left join utilizadores as u on(t.id_utilizador = u.id)
                where 
                    t.id_utilizador = :id_utilizador
                order by
                    t.data desc
                    
            ';

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':id_utilizador', $this->__get('id_utilizador'));
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        


    }



?>