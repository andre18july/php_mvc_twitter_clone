<?php

    namespace App\Models;

    use MF\Model\Model;

    class Utilizador extends Model{

        private $id;
        private $nome;
        private $email;
        private $senha;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }

        //gravar
        public function gravarUtilizador(){
            $query = "
                insert 
                    into utilizadores(nome, email, senha) 
                    values(:nome, :email, :senha)
            ";

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':nome', $this->__get('nome'));
            $stmt->bindValue(':email', $this->__get('email'));
            $stmt->bindValue(':senha', $this->__get('senha'));
            $stmt->execute();

            return $this;
        }
        
        //validar se um cadastro pode ser feito
        public function validarRegisto(){
            $valido = true;
            $regexEmail = "/[a-zA-Z0-9_-.+]+@[a-zA-Z0-9-]+.[a-zA-Z]+/";
            $error = "";

            $nome = $this->__get('nome');
            if(strlen($nome)<3){
                $valido = false;
                $error .= "<br>Nome inválido";
            }

            $email = $this->__get('email');
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $valido = false;
                $error .= "<br>Email inválido";
            }

            $senha = $this->__get('senha');
            if(strlen($senha)<8){
                $valido = false;
                $error .= "<br>Senha inválida";
            }

            return $error;
        }


        //recuperar um utilizador por email
        public function obterUtilizadorPorEmail(){
            $query = "
                select 
                    nome, email
                from
                    utilizadores
                where
                    email = :email
            ";

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':email', $this->__get('email'));
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }

        public function autenticar(){
            $query = "
                select 
                    id, nome, email
                from
                    utilizadores
                where
                    email = :email and senha = :senha
            ";

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':email', $this->__get('email'));
            $stmt->bindValue(':senha', $this->__get('senha'));
            $stmt->execute();

            $utilizador = $stmt->fetch(\PDO::FETCH_ASSOC);

            /*
            echo "<pre>";
            print_r($utilizador);
            echo "</pre>";
            */
                        
            if($utilizador['id'] != '' && $utilizador['nome'] != ''){
                $this->__set('id', $utilizador['id']);
                $this->__set('nome', $utilizador['nome']);

            }

        }


        public function obterUtilizadoresPorPesquisa(){
            $query = '
                select 
                    u.id, u.nome, u.email,
                    (
                        select
                            count(*)
                        from
                            utilizadores_seguidores as us
                        where us.id_utilizador = :id_currentUser
                            and
                                us.id_utilizador_seguindo = u.id
                    ) as seguindo_sn
                from
                    utilizadores u
                where
                    u.nome like :nome and u.id != :id_currentUser
            ';

        
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':nome', '%'. $this->procurar .'%');
            $stmt->bindValue(':id_currentUser', $_SESSION['id']);
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
        }


        public function seguirUtilizador($id_utilizador_seguindo){
            //echo "seguirUtilizador id utilizador_seguindo: " . $id_utilizador_seguindo;
            $query = '
                insert into 
                    utilizadores_seguidores(id_utilizador, id_utilizador_seguindo)
                values(:id_utilizador, :id_utilizador_seguindo)
            ';

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':id_utilizador', $this->__get('id'));
            $stmt->bindValue(':id_utilizador_seguindo', $id_utilizador_seguindo);
            $stmt->execute();

            return true;
        }

        public function naoSeguirUtilizador($id_utilizador_seguindo){
            //echo "naoSeguirUtilizador id utilizador_seguindo: " . $id_utilizador_seguindo;

            $query = '
                delete from
                    utilizadores_seguidores
                where
                    id_utilizador = :id_utilizador 
                and 
                    id_utilizador_seguindo = :id_utilizador_seguindo
            ';

            $stmt = $this->db->prepare($query);
            $stmt->bindValue(':id_utilizador', $this->__get('id'));
            $stmt->bindValue(':id_utilizador_seguindo', $id_utilizador_seguindo);
            $stmt->execute();

            return true;
        }


    }


?>