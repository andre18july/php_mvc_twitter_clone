<?php

    namespace App\Controllers;

    use MF\Controller\Action;
    use MF\Model\Container;

    class AppController extends Action{

        public function timeline(){

            $this->validarAutenticacao();
                
            $tweet = Container::getModel('tweet');
            $tweet->__set('id_utilizador', $_SESSION['id']);
            $tweets = $tweet->listar();

            /*
            echo "<pre>";
            print_r($tweets);
            echo "</pre>";
            */

            $this->view->tweets = $tweets;
            
            $this->render("timeline");
                
            
        }

        public function tweet(){

            $this->validarAutenticacao();
                
            $tweet = Container::getModel('tweet');

            $tweet->__set('tweet', $_POST['tweet']);
            $tweet->__set('id_utilizador', $_SESSION['id']);
                            
            $tweet->gravar();

            header('location: /timeline');
            

        }

        public function validarAutenticacao() {

            session_start();

            if(!isset($_SESSION['id']) || $_SESSION['id'] == '' || !isset($_SESSION['nome']) || $_SESSION['nome'] == ''){
                header('location: /?login=erro');
            }else{
                return true;
            }
        }


        public function quemSeguir(){

            $this->validarAutenticacao();
           

            /*
            echo "<pre>";
            print_r($_GET);
            echo "</pre>";
            */

            $procurar = isset($_GET['procurar']) ? $_GET['procurar'] : '';

            $utilizadores = array();

            if($procurar != ''){
                $utilizador = Container::getModel('Utilizador');
                $utilizador->procurar = $procurar;

                /*
                echo "<pre>";
                print_r($utilizador);
                echo "</pre>";
                */

                $utilizadores = $utilizador->obterUtilizadoresPorPesquisa();
                
                /*
                echo "<pre>";
                print_r($utilizadores);
                echo "</pre>";
                */



            }

            $this->view->utilizadores = $utilizadores;
    
            $this->render("quemSeguir");


        }


        public function accao(){

            $this->validarAutenticacao();

            
            echo "<pre>";
            print_r($_GET);
            echo "</pre>";

            $accao = isset($_GET['accao']) ? $_GET['accao'] : '';
            $id_utilizador_seguindo = isset($_GET['utilizador']) ? $_GET['utilizador'] : '';
            
            $utilizador = Container::getModel('Utilizador');
            $utilizador->__set('id', $_SESSION['id']);

     
            if($_GET['accao'] == 'seguir'){
                //echo "Accao = seguir user";
                $utilizador->seguirUtilizador($id_utilizador_seguindo);

            }else if($_GET['accao'] == 'naoseguir'){
                //echo "Accao = nao seguir user";
                $utilizador->naoSeguirUtilizador($id_utilizador_seguindo);
            }
            
            header('location: /quem_seguir');

        }

        
    }

?>