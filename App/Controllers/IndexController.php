<?php

    namespace App\Controllers;

    use MF\Controller\Action;
    use MF\Model\Container;


    class IndexController extends Action{

        public function index(){

            $this->view->login = '';
            if(isset($_GET['login']) && $_GET['login'] == 'erro'){
                $this->view->login = 'erro';
            }
            
            $this->render('index');
        }

        public function registo(){
            $this->view->erroRegisto = false;
            $this->view->utilizador = array(
                'nome' => '',
                'email' => '',
                'senha' => ''
            );
            $this->render('registo');
        }

        public function registar(){
         

            $utilizador = Container::getModel('Utilizador');
            $utilizador->__set('nome', $_POST['nome']);
            $utilizador->__set('email', $_POST['email']);
         
            $utilizador->__set('senha', md5($_POST['senha'])); 
            
            if($utilizador->validarRegisto() === '' && count($utilizador->obterUtilizadorPorEmail()) == 0){
                
                $utilizador->gravarUtilizador();
                $this->render("cadastro");
                
            }else{
                $this->view->utilizador = array(
                    'nome' => $_POST['nome'],
                    'email' => $_POST['email'],
                    'senha' => $_POST['senha']
                );
                $this->view->erroRegisto = true;
                $this->view->erros = $utilizador->validarRegisto();
                $this->render("registo");
            }
        }   

    }
?>