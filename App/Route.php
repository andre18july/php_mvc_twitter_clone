<?php

    namespace App;

    use MF\Init\Bootstrap;

    class Route extends Bootstrap{

       
        protected function initRoutes(){

            $routes['home'] = array(
                'route' => '/',
                'controller' => 'indexController',
                'action' => 'index'
            );

            $routes['registo'] = array(
                'route' => '/registo',
                'controller' => 'indexController',
                'action' => 'registo'
            );

            $routes['registar'] = array(
                'route' => '/registar',
                'controller' => 'indexController',
                'action' => 'registar'
            );

            $routes['autenticar'] = array(
                'route' => '/autenticar',
                'controller' => 'AuthController',
                'action' => 'autenticar'
            );

            $routes['timeline'] = array(
                'route' => '/timeline',
                'controller' => 'AppController',
                'action' => 'timeline'
            );

            $routes['sair'] = array(
                'route' => '/sair',
                'controller' => 'AuthController',
                'action' => 'sair'
            );

            $routes['tweet'] = array(
                'route' => '/tweet',
                'controller' => 'AppController',
                'action' => 'tweet'
            );

            $routes['quem_seguir'] = array(
                'route' => '/quem_seguir',
                'controller' => 'AppController',
                'action' => 'quemSeguir'
            );

            $routes['acccao'] = array(
                'route' => '/accao',
                'controller' => 'AppController',
                'action' => 'accao'
            );



            $this->setRoutes($routes);

        }

    }

?>